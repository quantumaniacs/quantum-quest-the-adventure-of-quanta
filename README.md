# Quantum Quest The Adventure of Quanta

**Team Name:** Quantumaniacs

**Members:** Angel Contreras, Cesar Mallaupoma, Kevin Durán, Antonia Morales, Miguel Luna, and Carolina Perdomo.

**Game description:**  
“Quantum Quest The Adventure of Quanta” is a creation by Quantumaniacs, aiming to blend the thrill of video games with the fascinating world of quantum computing. This game is an entry point for young minds to grasp complex concepts like superposition, Grover's algorithm, decoherence, entanglement, and others; through an interactive narrative. To continue during the QJam 2024, it stands as a testament to the educational potential of gaming.

**Tools:** Ren'Py, Python, Leonardo.Ai and dalle-3.

**Platform:** Computer (Windows, Linux, Mac).

**Project repo:** https://gitlab.com/quantumaniacs/quantum-quest-the-adventure-of-quanta

**Demo video** (Updated on Tuesday 16/01/24): https://youtu.be/pJpPDhdSpK0

**Installation video** (Updated on Tuesday 16/01/24): https://youtu.be/4c4ZOlktyrw

**Project Explanation**: https://youtu.be/Z_BEEvnyGL0

**itch.io link**: https://carolinaperdomo.itch.io/quantum-quest-the-adventure-of-quanta
